package assignments;

import java.util.Scanner;

public class SumOfNumbersInArray {

	public static void main(String[] args) {
		
		Scanner scan  = new Scanner(System.in);
		System.out.println("Enter the Numbers");
		int[] numbers= new int[6];
		int sum=0;
		for(int i=0;i<6;i++) {
			
			numbers[i]=scan.nextInt();
			System.out.println("Index value:"+i);
			System.out.println("I value:"+numbers[i]);
			
		}
		for(int arr:numbers) {
			sum= sum+arr;
		}
		System.out.println("Sum of the Array numbers:"+sum);
	}
	
	
	
	/* public static void main(String args[]){
	      Scanner scanner = new Scanner(System.in);
	      int[] array = new int[6];
	      int sum = 0;
	      System.out.println("Enter the elements:");
	      for (int i=0; i<6; i++)
	      {
	    	  array[i] = scanner.nextInt();
	      }
	      for( int num : array) {
	          sum = sum+num;
	      }
	      System.out.println("Sum of array elements is:"+sum);
	   }*/
}

