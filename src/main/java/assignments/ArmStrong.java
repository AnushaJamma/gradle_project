package assignments;

public class ArmStrong {

	public static void main(String[] args) {

		for (int i = 100; i <= 1000; i++) {

			int num = 0;
			int sum = 0;
			int digit = i;

			while (num > 0) {

				digit = num % 10;
				sum = sum + (digit * digit * digit);
				num = num / 10;
			}

			if (sum == i) {
				System.out.println(i + " Number is Armstrong ");
			}
			sum = 0;
		}
	}

}
