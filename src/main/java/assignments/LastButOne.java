package assignments;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LastButOne {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		// Create object for Chrome Driver

		ChromeDriver driver = new ChromeDriver();

		driver.get("http://leafground.com/pages/Dropdown.html");
		driver.manage().window().maximize();

		Map<Integer,String> treeMap = new TreeMap<>();
		
		WebElement trainingProgramDropDown = driver.findElementById("dropdown1");

		Select selObj = new Select(trainingProgramDropDown);
		
		List<WebElement> trainingOptions = selObj.getOptions();
		
		for(int i=0;i<trainingOptions.size();i++) {
			treeMap.put(i, trainingOptions.get(i).getText());
			
		}
		System.out.println(treeMap);
	System.out.println(treeMap.get(treeMap.size()-2));
}

}
