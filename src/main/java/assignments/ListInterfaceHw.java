package assignments;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class ListInterfaceHw {

	public static void main(String[] args) {

		List<String> arrList= new ArrayList<>();
		
		arrList.add("Anusha");
		arrList.add("Monisha");
		arrList.add("Aruna");
		arrList.add("Anusha");
		arrList.add("Anusha");
		arrList.add("Lavanya");
		arrList.add("Lavanya");
		
		System.out.println("List Size:"+arrList.size());
		Collections.sort(arrList);
		System.out.println(arrList);
		
		Set<String> setObj = new TreeSet<>();
		System.out.println("Add all the values to Set");
		setObj.addAll(arrList);
		System.out.println("After Remove Duplicates:"+setObj);
		
}
}