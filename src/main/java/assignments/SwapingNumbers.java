package assignments;

public class SwapingNumbers {

	public static void main(String[] args) {
		
		int a=2;
		int b=3;
		
		System.out.println("Before Swap a:"+ a);
		System.out.println("Before Swap b:"+ b);

		
		a=a+b;//2+3=5 so a=5
		b=a-b;//5-3=2 so b=2
		a=a-b;//5-2=3 so a=3
		
		System.out.println("After Swap a:"+ a);
		System.out.println("After Swap b:"+ b);
	}

}
