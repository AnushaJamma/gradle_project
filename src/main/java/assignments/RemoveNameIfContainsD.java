package assignments;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RemoveNameIfContainsD {

	public static void main(String[] args) {

		List<String> listEmpNames = new ArrayList<>();
		listEmpNames.add("Dharma");
		listEmpNames.add("Anusha");
		listEmpNames.add("GovinD");
		listEmpNames.add("Divya");
		listEmpNames.add("Dharma");
		listEmpNames.add("GoDavari");
		listEmpNames.add("Chanti");

		System.out.println("List size before removing names:"+listEmpNames.size());
		/*if(listEmpNames.contains("D")) {
			listEmpNames.remove(listEmpNames);
			System.out.println("Emp names without contains D:"+ listEmpNames);
		}
		else {
			System.out.println("Emp names with contains D:"+ listEmpNames);
		}*/


		for(int i=0;i<listEmpNames.size();i++) {
			if(listEmpNames.get(i).contains("D")) {
				listEmpNames.remove(listEmpNames.get(i));
				System.out.println("Emp names without contains D:"+listEmpNames);

			}
			else {
				System.out.println("Emp names with contains D:"+ listEmpNames);	

		}
			}
		Collections.sort(listEmpNames);
		System.out.println("Emp list in sorting order:"+ listEmpNames);
	}

}
