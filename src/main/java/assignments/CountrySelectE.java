package assignments;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CountrySelectE {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();

		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.findElementByXPath("(//span[text()='AGENT LOGIN'])[1]").click();
		driver.findElementByLinkText("Sign up").click();

		//select Egypt from the Country drop down not Ecuador

		WebElement countryDropdown = driver.findElementByXPath("//select[@id='userRegistrationForm:countries']");
		Select country= new Select(countryDropdown);
		List<WebElement> options = country.getOptions();
		String[] strOptions= new String[options.size()];
		for(int i=0;i<options.size();i++) {
			if(options.get(i).getText().startsWith("Eg")) {
				System.out.println(options.get(i).getText());
				Thread.sleep(2000);
			}
		}
		country.selectByValue("56");
		Thread.sleep(2000);
		driver.close();
	}
}
