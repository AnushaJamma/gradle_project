package assignments;

import org.openqa.selenium.chrome.ChromeDriver;

public class CheckBoxIsSelected {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		// Create object for Chrome Driver

		ChromeDriver driver = new ChromeDriver();

		/*	driver.get("https://erail.in/");
		driver.manage().window().maximize();

		driver.findElementByXPath("//input[@id='chkSelectFromOnly']").click();//checkbox check

		boolean checkIsSelected = driver.findElementByXPath("//input[@id='chkSelectDateOnly']").isSelected();

		if(checkIsSelected=true ) {
			System.out.println("checkbox is checked");
		}

		else {
			System.out.println("checkbox not checked");
		}*/

		driver.get("http://www.leafground.com/pages/checkbox.html");
		driver.manage().window().maximize();
		driver.findElementByXPath("//input[@type='checkbox'][1]").click();
		boolean checkBoxCheck = driver.findElementByXPath("//input[@type='checkbox'][1]").isSelected();

		if(checkBoxCheck=true) {
			System.out.println("Check Box selected");
		}
		else
		{
			System.out.println("Check Box not selected");
		}
		driver.close();
	}

}


