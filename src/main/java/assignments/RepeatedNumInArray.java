package assignments;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class RepeatedNumInArray {

	public static void main(String[] args) {
		int[] a = { 13, 65, 15, 67, 88, 65, 13, 99, 67, 13, 65, 87, 13 };
		Set<Integer> ts = new TreeSet<>();
		for (int i = 0; i < a.length; i++) {

			for (int j = i + 1; j < a.length; j++) {
				if (a[i] == a[j])
					ts.add(a[j]);
			}
		}
		System.out.print(ts + ":are duplcate values ");
	}
}
