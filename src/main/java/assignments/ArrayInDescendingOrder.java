package assignments;

import java.util.Arrays;
import java.util.Collections;

public class ArrayInDescendingOrder {

	public static void main(String[] args) {

		Integer[] arr= {13,25,9,56,54,89,17};
		Arrays.sort(arr,Collections.reverseOrder());
		for(int a:arr){
			System.out.println(a);
		}
	}
}


