package Week5.day2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


public class LeavelZoomCar {

	public static void main(String[] args) throws InterruptedException {


		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();

		driver.get("https://www.zoomcar.com/chennai");

		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.findElementByXPath("(//div[@class='newHomeSearch'])").click();//click search
		Thread.sleep(2000);
		driver.findElementByXPath("(//div[@class='items'])[3]").click();//items	
		driver.findElementByXPath("//button[text()='Next']").click();//click next
		Thread.sleep(2000);
		String firstDate = driver.findElementByXPath("//div[text()='Mon']").getText();//firstDate
		System.out.println("FirstDate:"+firstDate);
		driver.findElementByXPath("//div[text()='Mon']").click();
		driver.findElementByXPath("//button[text()='Next']").click();
		Thread.sleep(3000);
		String secondDate = driver.findElementByXPath("//div[text()='Mon']").getText();//second Date
		System.out.println("seconddate:"+secondDate);
		driver.findElementByXPath("//div[text()='Mon']").click();
		
		if(firstDate.equals(secondDate)) {
			System.out.println("FirstDate:" + firstDate + " Its Matching ");
		}
		driver.findElementByXPath("//button[@class='proceed']").click();//click done
		
		driver.findElementByXPath("//div[@class='results-body']/div[2]/label/following::div[1]").click();
		
		String carName = driver.findElementByXPath("(//div[@class='details']/h3)[1]").getText();
		System.out.println(carName);
		String price =driver.findElementByXPath("((//div[@class='car-listing']/div)[1])//div[@class='price']").getText();
		
		price = price.replaceAll("[^a-zA-Z0-9]", "");
		
		System.out.println("$" + price);
		
		//driver.close();

	}

}










