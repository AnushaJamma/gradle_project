package Week5.day2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class FaceBook {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		ChromeOptions op = new ChromeOptions();
		op.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(op);

		//ChromeDriver driver= new ChromeDriver();


		driver.get("https://www.facebook.com/");

		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.manage().window().maximize();

		driver.findElementByXPath("//input[@id='email']").sendKeys("syed.akthar52@gmail.com");
		driver.findElementByXPath("//input[@id='pass']").sendKeys("ASAaktr117");
		driver.findElementByXPath("//input[@type='submit']").click();
		driver.findElementByXPath("//input[@type='text'][2]").sendKeys("TestLeaf");
		driver.findElementByXPath("(//button[@type='submit'])[1]").click();//search
		boolean textTestLeaf = driver.findElementByXPath("//a[@class='_2yez'][1]").isDisplayed();
		System.out.println(" "+ textTestLeaf);
		Thread.sleep(2000);
		String likeButtonText = driver.findElementByXPath("(//button[@type='submit'])[2]").getText();
		System.out.println("Capture the Text:"+likeButtonText);
		Thread.sleep(5000);
		if(likeButtonText.equals("Liked")) {
			System.out.println("Its Liked already");
		}
		else if(likeButtonText.equals("Like")) {
			driver.findElementByXPath("(//button[@type='submit'])[2]").click();
			System.out.println("Now page liked");
		}
		Thread.sleep(2000);
		driver.findElementByXPath("//div[@id='xt_uniq_4']/div[@class='_2yev']/div[@class='_2xjf']/a[@class='_2yez']").click();
		String title=driver.getTitle();
		if(title.contains("TestLeaf")) {
			System.out.println(title);
		}
		String numOfLikes = driver.findElementByXPath("(//div[@class='clearfix _ikh'])[1]/following::div[@class='clearfix _ikh'][1]/div[2]").getText();
		System.out.println(numOfLikes);
		//driver.close();
	}

}
