package Week5.day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdmethods.FaceBookMethods;

public class FaceBookUsingSeMethods extends FaceBookMethods{

	@BeforeClass
	
	public void faceBookDataSetUp() {
		
		testCaseName ="FaceBook";
		testDesc ="Hit Like To Test Leaf Page";
		author ="Anusha";
		category ="Smoke Testing";
	}
	
	@Test
	
	public void facebookSteps() throws InterruptedException {
		
		WebElement faceBookUserId=locateElement("xpath","//input[@id='email']");
		type(faceBookUserId,"jamma.anusha21@gmail.com");
		
		WebElement faceBookpassword=locateElement("xpath","//input[@id='pass']");
		type(faceBookpassword,"anukiranuday");
		
		WebElement clickLogin=locateElement("xpath","//input[@type='submit']");
		click(clickLogin);
		Thread.sleep(3000);
		
		WebElement enterTextInSearch=locateElement("xpath","//input[@type='text'][2]");
		type(enterTextInSearch,"TestLeaf");
		
		WebElement clickSearch=locateElement("xpath","(//button[@type='submit'])[1]");
		click(clickSearch);
		
		WebElement TestLeaftext=locateElement("xpath","//a[@class='_2yez'][1]");
		verifyDisplayed(TestLeaftext);
		
		WebElement likeButtonText=locateElement("xpath","(//button[@type='submit'])[2]");
		type(likeButtonText,"TestLeaf");
		
		Thread.sleep(2000);
		
		if(likeButtonText.equals("Liked")) {
			System.out.println("Its Liked already");
		}
		else if(likeButtonText.equals("Like")) {
			driver.findElementByXPath("(//button[@type='submit'])[2]").click();
			System.out.println("Now page liked");
		}
		
		WebElement clickLink=locateElement("xpath","//div[@id='xt_uniq_4']/div[@class='_2yev']/div[@class='_2xjf']/a[@class='_2yez']");
		click(clickLink);
		
		verifyTitle(" TestLeaf - Home ");
		
		WebElement numOfLikes=locateElement("xpath","(//div[@class='clearfix _ikh'])[1]/following::div[@class='clearfix _ikh'][1]/div[2]");
		getText(numOfLikes);
		
	}
	
	/*@AfterMethod
	
	public void closeChrome() {
		
		closeBrowser();
	}
	*/
	
}
