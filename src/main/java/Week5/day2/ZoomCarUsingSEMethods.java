package Week5.day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import wdmethods.ZoomCarMethods;


public class ZoomCarUsingSEMethods extends ZoomCarMethods {

	@BeforeClass

	public void zoomCarSetData() {
		
		testCaseName ="ZoomCar";
		testDesc ="List Of Zoom Cars";
		author ="Anusha";
		category ="Regression";
	}
	
	@Test
	
	public void StartBrowser() throws InterruptedException {

		WebElement search=locateElement("xpath","(//div[@class='newHomeSearch'])");
		click(search);
		
		Thread.sleep(2000);
		
		WebElement clickPlace=locateElement("xpath","(//div[@class='items'])[3]");
		click(clickPlace);
		
		WebElement clickNextButton=locateElement("xpath","//button[text()='Next']");
		click(clickNextButton);
		
		WebElement firstDate=locateElement("xpath","//div[text()='Mon']");
		click(firstDate);
		getText(firstDate);
		
		WebElement clickNextButton1=locateElement("xpath","//button[text()='Next']");
		click(clickNextButton1);
		
		WebElement secondDate=locateElement("xpath","//div[text()='Mon']");
		click(secondDate);
		getText(secondDate);
		
		if(firstDate.equals(secondDate)) {
			System.out.println("FirstDate:" + firstDate + " Its Matching ");
		}
		else
		{
			System.out.println("FirstDate:" + firstDate + " Its not Matching ");
		}
		
		WebElement clickDone=locateElement("xpath","//button[@class='proceed']");
		click(clickDone);
		
		WebElement clickSortOrder=locateElement("xpath","//div[@class='results-body']/div[2]/label/following::div[1]");
		click(clickSortOrder);
		
		WebElement carName=locateElement("xpath","(//div[@class='details']/h3)[1]");
		click(carName);
		getText(carName);
		
		String price= locateElement("xpath","((//div[@class='car-listing']/div)[1])//div[@class='price']").getText();
		price = price.replaceAll("[^a-zA-z0-9]","");
		
		getText("$"+price);
		
	}

	


	

}
