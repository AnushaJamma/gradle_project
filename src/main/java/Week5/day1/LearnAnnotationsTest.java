package Week5.day1;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.BeforeSuite;

public class LearnAnnotationsTest {

	
	@BeforeSuite
	 public void a() {
		System.out.println("wakup1");
	}
	
	@Test
	public void BmainTask() {
		System.out.println("mainTask");
	}
	
	@BeforeSuite

	public void b() {
		System.out.println("wakeUp");
	}

	@BeforeTest

	public void brushUp() {
		System.out.println("brushUP");
	}

	@BeforeClass

	public void getReady() {
		System.out.println("getReady");
	}

	@BeforeMethod

	public void getIntoOffice() {
	System.out.println("getIntoOffice");
	}
	
	@Test
	
	public void BmainTask1() {
		System.out.println("mainTask1");
	}
	
	@Test
	
	public void AmainTask2() {
		System.out.println("mainTask2");
	}
	
	@AfterMethod
	
	public void comeForBreak() {
		System.out.println("comeForBreak");
	}

	
	@AfterTest
	public void getOutOfOffice() {
		System.out.println("getOutOfOffice");
	}
	

	
	
	
	}

