package Week3.day2;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;


public class IRCTCLogin {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();

		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.findElementByXPath("(//span[text()='AGENT LOGIN'])[1]").click();
		driver.findElementByLinkText("Sign up").click();

		driver.findElementByXPath("//input[@id='userRegistrationForm:userName']").sendKeys("AnushaJamma");
		driver.findElementByXPath("//input[@id='userRegistrationForm:password']").sendKeys("Anu@75*sha");
		driver.findElementByXPath("//input[@id='userRegistrationForm:confpasword']").sendKeys("Anu@75*sha");
		WebElement securityDropDown = driver.findElementByXPath("//select[@id='userRegistrationForm:securityQ']");
		Select security= new Select(securityDropDown);
		security.selectByIndex(7);

		driver.findElementByXPath("//input[@id='userRegistrationForm:securityAnswer']").sendKeys("bike");

		WebElement prelanDropDown = driver.findElementByXPath("//select[@id='userRegistrationForm:prelan']");
		Select prelan= new Select(prelanDropDown);
		prelan.selectByIndex(0);

		driver.findElementByXPath("//input[@id='userRegistrationForm:firstName']").sendKeys("Anusha"); //enter First Name
		driver.findElementByXPath("//input[@id='userRegistrationForm:gender:1']").click(); //click gender radio button
		driver.findElementByXPath("//input[@id='userRegistrationForm:maritalStatus:1']").click(); //click maritalStatus radio button

		WebElement dateDropDown = driver.findElementByXPath("//select[@id='userRegistrationForm:dobDay']");
		Select date= new Select(dateDropDown);
		date.selectByIndex(6);

		WebElement monthDropDown = driver.findElementByXPath("//select[@id='userRegistrationForm:dobMonth']");
		Select month= new Select(monthDropDown);
		month.selectByIndex(3);

		WebElement yearDropDown = driver.findElementByXPath("//select[@id='userRegistrationForm:dateOfBirth']");
		Select year= new Select(yearDropDown);
		year.selectByIndex(3);


		WebElement occupationDropDown = driver.findElementByXPath("//select[@id='userRegistrationForm:occupation']");
		Select Occupation= new Select(occupationDropDown);
		Occupation.selectByIndex(4);

		WebElement countryDropdown = driver.findElementByXPath("//select[@id='userRegistrationForm:countries']");
		Select country= new Select(countryDropdown);
		country.selectByValue("94");

		driver.findElementByXPath("//input[@id='userRegistrationForm:email']").sendKeys("Grandhi.Anusha21@gmail.com");//enter email id
		driver.findElementByXPath("//input[@id='userRegistrationForm:mobile']").sendKeys("9708654560");

		WebElement nationalityDropDown = driver.findElementByXPath("//select[@id='userRegistrationForm:nationalityId']");
		Select nationality= new Select(nationalityDropDown);
		nationality.selectByVisibleText("India");

		driver.findElementByXPath("//input[@id='userRegistrationForm:address']").sendKeys("address");//enter address

		driver.findElementByXPath("//input[@id='userRegistrationForm:pincode']").sendKeys("600041",Keys.TAB);//enter pin code

		WebElement cityTownDropDown = driver.findElementByXPath("//select[@id='userRegistrationForm:cityName']");
		Select cityTown = new Select(cityTownDropDown);
		Thread.sleep(2000);
		cityTown.selectByIndex(2);

		WebElement postOfficeDropDown = driver.findElementByXPath("//select[@id='userRegistrationForm:postofficeName']");
		Select postOffic = new Select(postOfficeDropDown);
		Thread.sleep(2000);
		postOffic.selectByIndex(2);

		driver.findElementByXPath("//input[@id='userRegistrationForm:landline']").sendKeys("9708654560");
		driver.findElementByXPath("//input[@id='userRegistrationForm:resAndOff:1']").click();

		Thread.sleep(5000);

		 

	}

}
