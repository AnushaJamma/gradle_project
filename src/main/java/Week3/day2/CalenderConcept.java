package Week3.day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class CalenderConcept {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		// Create object for Chrome Driver

		ChromeDriver driver = new ChromeDriver();

		driver.get("http://leaftaps.com/opentaps");

		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.manage().window().maximize();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();// create lead

		driver.findElementByXPath("//img[@id='createLeadForm_birthDate-button']").click();
		driver.findElementByXPath("//div[@class='calendar']/table/thead/tr[@class='headrow']/td[4]").click();
		driver.findElementByXPath("//div[@class='calendar']/table/tbody/tr[3]/td[5]").click();

	}

}
