package Week3.day1;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Login {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		// Create object for Chrome Driver

		ChromeDriver driver = new ChromeDriver();

		driver.get("http://leaftaps.com/opentaps");

		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.manage().window().maximize();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();// create lead
		driver.findElementById("createLeadForm_companyName").sendKeys("BNY");
		driver.findElementById("createLeadForm_firstName").sendKeys("Anusha");
		driver.findElementById("createLeadForm_lastName").sendKeys("Jamma");

		driver.findElementByXPath("//input[@id='createLeadForm_firstNameLocal']").sendKeys("first Local");
		driver.findElementByXPath("//input[@id='createLeadForm_lastNameLocal']").sendKeys("last Local");
		driver.findElementByXPath("//input[@id='createLeadForm_personalTitle']").sendKeys("Salutation");

		WebElement Source = driver.findElementById("createLeadForm_dataSourceId");
		Select selByvisibleText = new Select(Source);
		selByvisibleText.selectByVisibleText("Employee");

		driver.findElementByXPath("//input[@id='createLeadForm_generalProfTitle']").sendKeys("Title");
		driver.findElementByXPath("//input[@id='createLeadForm_annualRevenue']").sendKeys("Annual Revenue");

		WebElement marketingCampaign = driver.findElementById("createLeadForm_marketingCampaignId");
		Select selByValue = new Select(marketingCampaign);
		selByValue.selectByValue("CATRQ_CARNDRIVER");

		WebElement industryDropDown = driver.findElementByXPath("//select[contains(@name,'industryEnumId')]");
		Select industry = new Select(industryDropDown);
		industry.selectByValue("IND_DISTRIBUTION");

		Thread.sleep(2000);
		List<WebElement> allOptions=industry.getOptions();
		for(int i=0;i<allOptions.size();i++) {
			System.out.println(allOptions.get(i).getText());
		}

		List<WebElement> industryAllOptions = industry.getOptions();
		for (int i = 0; i < industryAllOptions.size(); i++) {
			String strOption= industryAllOptions.get(i).getText();

			if (strOption.startsWith("C")) {
				System.out.println(industryAllOptions.get(i).getText());
			} 
		}

		
		WebElement OwnershipDropDown =driver.findElementByXPath("//select[contains(@id,'createLeadForm_ownershipEnumId')]");
		Select Ownership = new Select(OwnershipDropDown);
		Ownership.selectByValue("OWN_PARTNERSHIP");

		driver.findElementByXPath("//input[contains(@id,'createLeadForm_sicCode')]").sendKeys("sicCode");
		driver.findElementByXPath("//input[contains(@id,'createLeadForm_primaryPhoneCountryCode')]").sendKeys("CountryCode");
		driver.findElementByXPath("//input[contains(@id,'createLeadForm_primaryPhoneAreaCode')]").sendKeys("AreaCode");
		driver.findElementByXPath("//input[contains(@id,'createLeadForm_primaryPhoneExtension')]").sendKeys("PhoneExtension");
		driver.findElementByXPath("//input[contains(@id,'createLeadForm_departmentName')]").sendKeys("department");

		WebElement PreferredCurrencyDropDown= driver.findElementByXPath("//select[contains(@id,'createLeadForm_currencyUomId')]");
		Select PreferredCurrency = new Select(PreferredCurrencyDropDown);
		PreferredCurrency.selectByValue("AMD");

		driver.findElementByXPath("//input[contains(@id,'createLeadForm_numberEmployees')]").sendKeys("Employees");
		driver.findElementByXPath("//input[contains(@id,'createLeadForm_tickerSymbol')]").sendKeys("tickerSymbol");

		driver.findElementByXPath("//input[contains(@id,'createLeadForm_primaryPhoneNumber')]").sendKeys("9867564534");

		driver.findElementByXPath("//input[contains(@id,'createLeadForm_primaryPhoneAskForName')]").sendKeys("PhoneAskForName");
		driver.findElementByXPath("//input[contains(@id,'createLeadForm_primaryWebUrl')]").sendKeys("PhoneAskForName");

		driver.findElementByClassName("smallSubmit").click();
		driver.findElementByXPath("//a[text()='Find Leads']").click(); // using partial Relative Text based, with
		// Exact match

		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("Anusha");// Relative Attribute
		// based,with Exact match
		driver.findElementByXPath("(//input[@name='lastName'])[3]").sendKeys("Jamma");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		driver.findElementByXPath("(//a[contains(@class,'linktext')])[4]").click();
		driver.findElementByXPath("//a[text()='Edit']").click();
		Thread.sleep(3000);
		driver.findElementByXPath("//textarea[contains(@id,'updateLeadForm_description')]").clear();
		driver.findElementByXPath("//textarea[contains(@id,'updateLeadForm_description')]").sendKeys("Description");// using Contains Attribute
		driver.findElementByXPath("(//textarea[starts-with(@id,'updateLeadForm')])[2]").clear();
		driver.findElementByXPath("(//textarea[starts-with(@id,'updateLeadForm')])[2]").sendKeys("important note");// using Starts-with Attribute
		driver.findElementByXPath("//input[contains(@name,'submitButton')][1]").click();
		Thread.sleep(1000);
		driver.findElementByXPath("//a[text()='Duplicate Lead']").click();
		driver.findElementByXPath("//input[contains(@id,'createLeadForm_numberEmployees')]").sendKeys("Employees");
		driver.findElementByXPath("//input[contains(@id,'createLeadForm_tickerSymbol')]").sendKeys("$");
		driver.findElementByClassName("smallSubmit").click();

		driver.findElementByXPath("//a[text()='Find Leads']").click();
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("Anusha");
		driver.findElementByXPath("//button[text()='Find Leads']").click();//check the duplicate value
		driver.findElementByXPath("//input[@name='id']").clear();
		driver.findElementByXPath("//input[@name='id']").sendKeys("11659");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("(//a[@class='linktext'])[4]").click();

		driver.findElementByXPath("//a[text()='Delete']").click();

		driver.findElementByXPath("//a[text()='Find Leads']").click();
		driver.findElementByXPath("//input[@name='id']").sendKeys("11659");
		driver.findElementByXPath("//button[text()='Find Leads']").click();

		Thread.sleep(2000);

		//driver.close();

	}

}
