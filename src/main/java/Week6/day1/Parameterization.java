package Week6.day1;

import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;
import wdmethods.SeMethods;

public class Parameterization extends ProjectMethods {

	@Test(dataProvider="Fetch Data")
	
	public void createLead(String fName,String lName,String cName) throws InterruptedException {
	login();

	//WebElement eleCRMLink= locateElement("xpath", "//a[contains(text(),'CRM/SFA')]");
	click(locateElement("xpath", "//a[contains(text(),'CRM/SFA')]"));

	WebElement eleClickLeads= locateElement("xpath","//a[text()='Leads']");
	click(eleClickLeads);

	WebElement eleClickCreateLead= locateElement("xpath","//a[contains(text(),'Create Lead')]");
	click(eleClickCreateLead);

	//WebElement eleCompanyName= locateElement("id","createLeadForm_companyName");
	type(locateElement("id","createLeadForm_companyName"),fName);

	WebElement eleFirstName= locateElement("id","createLeadForm_firstName");
	type(eleFirstName,lName);

	WebElement eleLastName=locateElement("id","createLeadForm_lastName");
	type(eleLastName,cName);

		
	}
	
	@DataProvider(name="Fetch Data")
		public String[][] getData() {
			
			String[][] data= new String[2][3];
			
			data[0][0]="Anusha";
			data[0][1]="Jamma";
			data[0][2]="BNY";
			
			data[1][0]="Moni";
			data[1][1]="Prasad";
			data[1][2]="BNYM";
			
			return data;
			
			
			
		}
		
		
		
	}

