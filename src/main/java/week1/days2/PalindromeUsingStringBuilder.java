package week1.days2;

import java.util.Scanner;

public class PalindromeUsingStringBuilder {

	public static void main(String[] args) {


		Scanner scan = new Scanner(System.in);

		System.out.println("Enter a String Value");

		String input = scan.next();

		StringBuilder strBuilder= new StringBuilder(input);

		String rev =strBuilder.reverse().toString();
		System.out.println(rev);

		if(rev.equals(input)) {

			System.out.println("Given String "+ input + " Is a Palindrome ");

		}
		else {
			System.out.println("Given String " + input + " Is not a Palindrome ");
		}
	}

}
