package testCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;
import wdmethods.SeMethods;

public class DeleteLead extends ProjectMethods{

	@BeforeClass
	
	public void deleteLeadSetData() {
		
		testCaseName ="DeleteLead";
		testDesc ="DeleteLead Description";
		author ="Anusha";
		category ="Regression";
	}
	@Test

	public void DeleteLeads() throws InterruptedException {

		/*	startApp("chrome","http://leaftaps.com/opentaps/");

		WebElement userName=locateElement("id", "username");
		type(userName,"DemoSalesManager");

		WebElement password=locateElement("id", "password");
		type(password,"crmsfa");

		WebElement eleLogin = locateElement("xpath", "//input[@class='decorativeSubmit']");//click login button
		click(eleLogin); 
		 */

		WebElement eleCRMLink= locateElement("xpath", "//a[contains(text(),'CRM/SFA')]");
		click(eleCRMLink);//click CRM link

		WebElement eleClickLeads= locateElement("xpath","//a[text()='Leads']");
		click(eleClickLeads);

		WebElement clickfindLeads=locateElement("linkText","Find Leads");
		click(clickfindLeads);

		WebElement clickPhone=locateElement("xpath","//span[text()='Phone']");
		click(clickPhone);

		WebElement phoneCode=locateElement("xpath","//input[@name='phoneAreaCode']");
		type(phoneCode,"91");

		WebElement phoneNumber=locateElement("xpath","//input[@name='phoneNumber']");
		type(phoneNumber,"5454545444");

		WebElement findLeadsClick=locateElement("xpath","//button[text()='Find Leads']");
		click(findLeadsClick);

		Thread.sleep(2000);
		WebElement captureLeadId = locateElement("xpath","(//a[@class='linktext'])[4]");

		WebElement clickLeadId=locateElement("xpath","(//a[@class='linktext'])[4]");
		click(clickLeadId);

		WebElement clickDeleteButton=locateElement("xpath","//a[@class='subMenuButtonDangerous']");
		click(clickDeleteButton);

		WebElement againfindLeads=locateElement("linkText","Find Leads");
		click(againfindLeads);

		WebElement eleLeadId=locateElement("name","id");
		type(eleLeadId,"captureLeadId");

		WebElement clickFindLeadsButton=locateElement("xpath","//button[text()='Find Leads']");
		click(clickFindLeadsButton);


	}
}