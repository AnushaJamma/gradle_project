package testCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;
import wdmethods.SeMethods;

public class EditLeads extends ProjectMethods{
	
	@BeforeClass
	
	public void editLeadsSetData() {
		
		testCaseName ="EditLead";
		testDesc ="EditLead New Lead";
		author ="Anusha";
		category ="Regression";
	}

	//@Test(priority=2,invocationCount=2,invocationTimeOut=20000)
	@Test
	public void EditLead() throws InterruptedException {
		
		/*startApp("chrome","http://leaftaps.com/opentaps/");

		WebElement userName=locateElement("id", "username");
		type(userName,"DemoSalesManager");

		WebElement password=locateElement("id", "password");
		type(password,"crmsfa");

		WebElement eleLogin = locateElement("xpath", "//input[@class='decorativeSubmit']");//click login button
		click(eleLogin);*/ 

		WebElement eleCRMLink= locateElement("xpath", "//a[contains(text(),'CRM/SFA')]");
		click(eleCRMLink);//click CRM link

		WebElement eleClickLeads= locateElement("xpath","//a[text()='Leads']");
		click(eleClickLeads);

		WebElement clickfindLeads=locateElement("linkText","Find Leads");
		click(clickfindLeads);

		WebElement eleLeadId=locateElement("name","id");
		type(eleLeadId,"102");

		WebElement clickFindLeadsButton=locateElement("xpath","//button[text()='Find Leads']");
		click(clickFindLeadsButton);

		Thread.sleep(2000);

		WebElement leadListLink=locateElement("xpath","(//a[@class='linktext'])[4]");
		click(leadListLink);

		WebElement clickEditButton=locateElement("xpath","(//a[@class='subMenuButton'])[3]");
		click(clickEditButton);


		WebElement descriptionEdit=locateElement("id","updateLeadForm_description");
		descriptionEdit.clear();
		type(descriptionEdit,"Editing of Description");

		WebElement importantNoteEdit=locateElement("id","updateLeadForm_importantNote");	
		importantNoteEdit.clear();
		type(importantNoteEdit,"Editing of Important Note");

		WebElement changeCompanyName=locateElement("id","updateLeadForm_companyName");
		changeCompanyName.clear();
		type(changeCompanyName,"BNY");

		WebElement clickUpadteButton=locateElement("xpath","(//input[@class='smallSubmit'])[1]");
		click(clickUpadteButton);
		Thread.sleep(2000);
		
		WebElement editfindLeads=locateElement("linkText","Find Leads");
		click(editfindLeads);

		WebElement companyName=locateElement("xpath","(//input[@name='companyName'])[2]");
		type(companyName,"BNY");

		Thread.sleep(2000);

		WebElement leadList=locateElement("xpath","(//a[@class='linktext'])[4]");
		click(leadList);
		
		WebElement XactText=locateElement("xpath","//span[text()='Company Name']");
		
		verifyExactText(XactText, "BNY");
		
		driver.close();
	}
}
