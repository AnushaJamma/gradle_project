package testCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;
import wdmethods.SeMethods;

public class DuplicateLead extends ProjectMethods{
@BeforeClass
	
	public void createLeadSetData() {
		
		testCaseName ="DuplicateLead";
		testDesc ="Duplicate the Lead";
		author ="Anusha";
		category ="Regression";
	}

	@Test/*(groups="test.smoke")*/
	
	public void DuplicateLeads() throws InterruptedException {
		
		WebElement eleCRMLink= locateElement("xpath", "//a[contains(text(),'CRM/SFA')]");
		click(eleCRMLink);//click CRM link
		
		WebElement eleClickLeads= locateElement("xpath","//a[text()='Leads']");
		click(eleClickLeads);
		
		WebElement clickfindLeads=locateElement("linkText","Find Leads");
		click(clickfindLeads);
		Thread.sleep(1000);
		
		WebElement clickEmailTab=locateElement("xpath","//span[text()='Email']");
		click(clickEmailTab);
		
		WebElement enterEmail=locateElement("xpath","//input[@name='emailAddress']");
		type(enterEmail,"testSel@gmail.com");	
		
		WebElement FindLeadsButton=locateElement("xpath","//button[text()='Find Leads']");
		click(FindLeadsButton);
		
		Thread.sleep(2000);
		
		WebElement leadListLink=locateElement("xpath","(//a[@class='linktext'])[4]");
		click(leadListLink);
		Thread.sleep(1000);
		
		WebElement clickDuplicateLead=locateElement("xpath","//a[@class='subMenuButton'][1]");
		click(clickDuplicateLead);
		
		WebElement tiltle=locateElement("xpath","(//div[@class='x-panel-tc'])[22]");
		
		verifyTitle("tiltle");
		
		WebElement clickCreateLead=locateElement("name","submitButton");
		click(clickCreateLead);
		

}
}
