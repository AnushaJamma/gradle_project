package testCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;
import wdmethods.SeMethods;

public class CreateLead extends ProjectMethods{

	@BeforeClass
	
	public void createLeadSetData() {
		
		testCaseName ="CreateLead";
		testDesc ="Create New Lead";
		author ="Anusha";
		category ="Regression";
	}
	
	/*@BeforeMethod

	public void loginBrowser() throws InterruptedException {
		login();
	}*/

	//@Test(priority=3,invocationCount=2,invocationTimeOut=20000)
	
	@Test
	public void CreateLeadMethod() throws InterruptedException {

		/*startApp("chrome", "http://leaftaps.com/opentaps/");

		WebElement eleUsername = locateElement("id", "username");
		type(eleUsername, "DemoSalesManager");

		WebElement elePassword = locateElement("xpath", "//input[@id='password']");
		type(elePassword, "crmsfa");

		WebElement eleLogin = locateElement("xpath", "//input[@class='decorativeSubmit']");
		click(eleLogin); */
		//login();

		WebElement eleCRMLink= locateElement("xpath", "//a[contains(text(),'CRM/SFA')]");
		click(eleCRMLink);

		WebElement eleClickLeads= locateElement("xpath","//a[text()='Leads']");
		click(eleClickLeads);

		WebElement eleClickCreateLead= locateElement("xpath","//a[contains(text(),'Create Lead')]");
		click(eleClickCreateLead);

		WebElement eleCompanyName= locateElement("id","createLeadForm_companyName");
		type(eleCompanyName,"BNY Mellon");

		WebElement eleFirstName= locateElement("id","createLeadForm_firstName");
		type(eleFirstName,"AnushaVin");

		WebElement eleLastName=locateElement("id","createLeadForm_lastName");
		type(eleLastName,"Grandhi");

		WebElement sourceDropDown=locateElement("id","createLeadForm_dataSourceId");
		click(sourceDropDown);
		//selectDropDownUsingText(sourceDropDown, "Employee");

		selectDropDownUsingIndex(sourceDropDown, 4);

		WebElement industryDropDown=locateElement("id","createLeadForm_industryEnumId");
		selectDropDownUsingIndex(industryDropDown, 5);

		WebElement calender= locateElement("id","createLeadForm_birthDate-button");
		click(calender);

		WebElement clickNextButton=locateElement("xpath","//div[@class='calendar']/table/thead/tr[@class='headrow']/td[4]");
		click(clickNextButton);

		WebElement selectDate=locateElement("xpath","//div[@class='calendar']/table/tbody/tr[@class='daysrow']/td[5]");
		click(selectDate);		
		Thread.sleep(5000);

		WebElement clickCreateLead=locateElement("name","submitButton");
		click(clickCreateLead);
		Thread.sleep(2000);
	
	}

	@AfterMethod
	public void closingBrowser() {
		closeBrowser();
		
	}


}


