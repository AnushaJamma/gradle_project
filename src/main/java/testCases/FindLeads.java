package testCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;
import wdmethods.SeMethods;

public class FindLeads extends ProjectMethods {
	@BeforeClass

	public void findLeadSetData() {

		testCaseName ="FindLeads";
		testDesc ="Find Lead";
		author ="Anusha";
		category ="Regression";
	}
	//@Test(priority=1,invocationCount=2,invocationTimeOut=20000)
	@Test
	public void clickFindLead() throws InterruptedException {
		/*startApp("Chrome", "http://leaftaps.com/opentaps/");

		WebElement userName=locateElement("id", "username");
		type(userName,"DemoSalesManager");

		WebElement password=locateElement("id", "password");
		type(password,"crmsfa");

		WebElement eleLogin = locateElement("xpath", "//input[@class='decorativeSubmit']");//click login button
		click(eleLogin);*/ 

		//login();

		WebElement eleCRMLink= locateElement("xpath", "//a[contains(text(),'CRM/SFA')]");
		click(eleCRMLink);//click CRM link

		WebElement eleClickLeads= locateElement("xpath","//a[text()='Leads']");
		click(eleClickLeads);

		WebElement clickfindLeads=locateElement("linkText","Find Leads");
		click(clickfindLeads);

		WebElement eleLeadId=locateElement("name","id");
		type(eleLeadId,"102");

		WebElement clickFindLeadsButton=locateElement("xpath","//button[text()='Find Leads']");
		click(clickFindLeadsButton);

		Thread.sleep(2000);

		WebElement leadListLink=locateElement("xpath","(//a[@class='linktext'])[4]");
		click(leadListLink);
	}
	@AfterMethod	
	public void closingBrowser() {
		closeBrowser();
	}
}
