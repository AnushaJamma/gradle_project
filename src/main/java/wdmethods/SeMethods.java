package wdmethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import Utils.ExtentReportInTestNG;

public class SeMethods extends ExtentReportInTestNG implements WdMethods {
	public RemoteWebDriver driver;
	int i =1;
	
	@Override
	public void startApp(String browser, String url) {
		try {
			if (browser.equalsIgnoreCase("chrome")) {
				
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				
				ChromeOptions op = new ChromeOptions();
				op.addArguments("--disable-notifications");
				driver = new ChromeDriver(op);
				
				//driver = new ChromeDriver();
			}
			else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();

			}
		}
		catch(WebDriverException e){
			System.out.println("Unknown Exception occured");
		}
		catch(Exception e) {
			System.out.println("Exception has occurced");
		}

		driver.manage().window().maximize();
		driver.get(url); 
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		System.out.println("The browser "+browser+" launched successfully"); 
		takeSnap();
	}

	@Override
	public WebElement locateElement(String locator, String locValue) {
		try {
			switch (locator) {
			case "id": return driver.findElementById(locValue);
			case "name": return driver.findElementByName(locValue);	
			case "xpath": return driver.findElementByXPath(locValue);
			case "class": return driver.findElementByClassName(locValue);
			case "linkText":return driver.findElementByLinkText(locValue);

			}
		} 
		catch (NoSuchElementException e) {
			System.out.println("The element is not found");
		}
		catch (WebDriverException e) {
			System.out.println("Unknown Exception occured");
		} 
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		return driver.findElementById(locValue);
	}

	@Override
	public void type(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data);
			//System.out.println("The data "+data+" enter successfully");
			reportSteps("pass","data entered"+ data+ "successfully");
		} 
		catch (WebDriverException e) {
			//System.out.println("The data "+data+" not enter successfully");
			reportSteps("fail","data entered"+ data+ "failed");
		}
		catch(Exception e) {
			//System.out.println("Exception has occurred");
			reportSteps("fail","data entered"+ data+ "failed");
		}
		finally {
			takeSnap();
		}
	}

	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			System.out.println("The element "+ele+" clicked"); 

			reportSteps("pass","data entered"+ ele+ "successfully");
		}
		catch(Exception e) {
			reportSteps("fail","data entered"+ ele+ "successfully");
		}
		finally {
			takeSnap();
		}
	}

	public void clickWithOutSnap(WebElement ele) {
		ele.click();
		System.out.println("The element "+ele+" clicked"); 

	}

	@Override
	public String getText(WebElement ele) {

		String text = ele.getText();
		System.out.println(text);
		return text;

	}
	
	protected void getText(String str) {
	
		return;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select eleSelectByText = new Select(ele);
			eleSelectByText.selectByVisibleText(value);
			System.out.println("The Value"+value+" is Selected in This"+ele);

		}
		catch(NoSuchElementException e) {
			System.out.println("No Such Element Exception has occurred");
		}
		takeSnap();
	}


	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
			Select eleSelectByIndex=new Select(ele);
			eleSelectByIndex.selectByIndex(index);
			System.out.println("DropDown value selected Using Index");
		}

		catch(NoSuchElementException e) {
			System.out.println("DropDown value not selected");
		}
		takeSnap();
		
	}
	@Override

	public boolean verifyTitle(String expectedTitle) {
		boolean bReturn=false;
		String title = driver.getTitle();

		if(title.equals(expectedTitle)) {
			System.out.println("The Title is matched"+expectedTitle);
			bReturn=true;
		}
		else {
			System.out.println("The Title is not  matched"+expectedTitle);
		}
		return bReturn;

	}


	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		try {

			if(ele.getText().contains(expectedText)) {
				System.out.println("The Title is matched"+expectedText);
			}
			else {
				System.out.println("The Title is not  matched"+expectedText);
			}
		}
		catch(Exception e) {
			System.out.println("Exception has occurred");
		}
		takeSnap();
	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {

		try {

			if(ele.getText().contains(expectedText)) {
				System.out.println("The Title is matched"+expectedText);
			}
			else {
				System.out.println("The Title is not  matched"+expectedText);
			}
		}
		catch(Exception e) {
			System.out.println("Exception has occurred");
		}

		takeSnap();
	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {

		try {
			if(ele.getAttribute(attribute).equals(value)) {
				System.out.println("The Title is matched");
			}
			else {
				System.out.println("The Title is not  matched");
			}
		}
		catch(Exception e) {
			System.out.println("The Exception Found");
		}
		takeSnap();
	}


	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		try {
			if(ele.getAttribute(attribute).equals(value)) {
				System.out.println("The Title is matched");
			}
			else {
				System.out.println("The Title is not  matched");
			}
		}
		catch(Exception e) {
			System.out.println("The Exception Found");
		}
		takeSnap();
	}

	@Override
	public void verifySelected(WebElement ele) {
		try {
			if(ele.isSelected()) {
				ele.click();
				System.out.println("Element is Selected");
			}
		}
		catch(Exception e) {
			System.out.println("Element is not Selected");
		}
		takeSnap();
	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		try {
			if(ele.isDisplayed()) {
				ele.click();
				System.out.println("Element is Displayed");
			}
		}
		catch(Exception e) {
			System.out.println("Element is not Displayed");
		}
		takeSnap();
	}

	@Override
	public void switchToWindow(int index) {
		try {
			Set<String> allWindows = driver.getWindowHandles();
			List<String> list=new ArrayList<>();
			list.addAll(allWindows);
			driver.switchTo().window(list.get(index));
		}
		catch(NoSuchWindowException e) {
			System.out.println("No Such Window Exception has occurred");
		}
		takeSnap();

	}

	@Override
	public void switchToFrame(WebElement ele) {
		try {
			driver.switchTo().frame(ele);
		}
		catch(NoSuchFrameException e) {
			System.out.println("No Such Frame Exception has occurred");
		}
		takeSnap();

	}

	@Override
	public void acceptAlert() {
		try {
			driver.switchTo().alert().accept();
			System.out.println("Accept Alert");
		}

		catch(NoAlertPresentException e){
			System.out.println("No Alert Present Exception");
		}

	}

	@Override
	public void dismissAlert() {
		try {
			driver.switchTo().alert().dismiss();
			System.out.println("dismissed Alert");
		}
		catch(NoAlertPresentException e) {
			System.out.println("No Alert Present Exception");
		}
		takeSnap();
	}

	@Override
	public String getAlertText() {
		try {
			return driver.switchTo().alert().getText();

		}
		catch(WebDriverException e) {
			System.out.println("WebDriver Exception has occurred");
		}
		return null;
	}

	@Override
	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dec = new File("./snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, dec);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		driver.close();
		System.out.println("closingBrowser successfully");
	}
	@Override
	public void closeAllBrowsers() {
		driver.quit();

	}

}
