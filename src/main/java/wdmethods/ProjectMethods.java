package wdmethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import Utils.ReadExcelData;


public class ProjectMethods extends SeMethods {
	
@DataProvider(name="Fecth Data")
	
	public String[][] getData() throws IOException {
		return ReadExcelData.getExcelData(excelsheetData);
	}

	@BeforeMethod

	public void login() throws InterruptedException {
		
		startApp("chrome", "http://leaftaps.com/opentaps/");
		
		WebElement eleUsername = locateElement("id", "username");
		type(eleUsername, "DemoCSR");

		WebElement elePassword = locateElement("xpath", "//input[@id='password']");
		type(elePassword, "crmsfa");

		WebElement eleLogin = locateElement("xpath", "//input[@class='decorativeSubmit']");
		click(eleLogin);

}
	
	
}