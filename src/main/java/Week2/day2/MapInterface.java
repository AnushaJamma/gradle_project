package Week2.day2;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class MapInterface {

	public static void main(String[] args) {
		String str ="Testleaf";
		char[] ch =str.toCharArray();
		
		Map<Character,Integer> mapObj = new TreeMap<>();
		for(char items: ch) {
			if(mapObj.containsKey(items)) {
				mapObj.put(items, mapObj.get(items)+ 1);
			}
			else {
				mapObj.put(items, 1);
			}
	}
		System.out.println(mapObj);
	}

}
