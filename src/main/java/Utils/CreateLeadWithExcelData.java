package Utils;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;

public class CreateLeadWithExcelData extends ProjectMethods {

@BeforeClass
	
	public void createLeadSetData() {
		
		testCaseName ="CreateLead";
		testDesc ="Create New Lead";
		author ="Anusha";
		category ="Regression";
		excelsheetData="ReadExcelData";
	}
	
	@Test
	public void createLeadMethodExcelData() throws InterruptedException {

		WebElement eleCRMLink= locateElement("xpath", "//a[contains(text(),'CRM/SFA')]");
		click(eleCRMLink);

		WebElement eleClickLeads= locateElement("xpath","//a[text()='Leads']");
		click(eleClickLeads);

		WebElement eleClickCreateLead= locateElement("xpath","//a[contains(text(),'Create Lead')]");
		click(eleClickCreateLead);

		WebElement eleCompanyName= locateElement("id","createLeadForm_companyName");
		//type(eleCompanyName,"BNY Mellon");

		WebElement eleFirstName= locateElement("id","createLeadForm_firstName");
		//type(eleFirstName,"AnushaVin");

		WebElement eleLastName=locateElement("id","createLeadForm_lastName");
		//type(eleLastName,"Grandhi");

		WebElement sourceDropDown=locateElement("id","createLeadForm_dataSourceId");
		click(sourceDropDown);
		//selectDropDownUsingText(sourceDropDown, "Employee");

		selectDropDownUsingIndex(sourceDropDown, 4);

		WebElement industryDropDown=locateElement("id","createLeadForm_industryEnumId");
		selectDropDownUsingIndex(industryDropDown, 5);

		WebElement calender= locateElement("id","createLeadForm_birthDate-button");
		click(calender);

		WebElement clickNextButton=locateElement("xpath","//div[@class='calendar']/table/thead/tr[@class='headrow']/td[4]");
		click(clickNextButton);

		WebElement selectDate=locateElement("xpath","//div[@class='calendar']/table/tbody/tr[@class='daysrow']/td[5]");
		click(selectDate);		
		Thread.sleep(5000);

		WebElement clickCreateLead=locateElement("name","submitButton");
		click(clickCreateLead);
		Thread.sleep(2000);
	
	}

	@AfterMethod
	public void closingBrowser() {
		closeBrowser();
		
	}


}

