package Utils;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentReportHardCoded {

	public void startResult() throws IOException {

		ExtentHtmlReporter html= new ExtentHtmlReporter("./Reports/Result.html");
		html.setAppendExisting(true);
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(html);
		
		ExtentTest test = extent.createTest("CreateLead", "Create new Lead");
		test.assignAuthor("Anusha");
		test.assignCategory("Regression");
		test.pass("Step:1 passed",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img.png").build());
		test.fail("Step:2 failed",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img.png").build());
		test.warning("Step:3 warning",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img.png").build());
		extent.flush();

	}

}
