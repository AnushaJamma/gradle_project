package Utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;

public class ReadExcelData extends ProjectMethods{
	@BeforeClass
	
	public void createLeadSetData() {
		
		testCaseName ="CreateLead";
		testDesc ="Create New Lead";
		author ="Anusha";
		category ="Regression";
		excelsheetData="ReadExcelData";
	}
	

	@Test
	public static String[][] getExcelData(String excelsheetData) throws IOException {

		XSSFWorkbook myBook = new XSSFWorkbook("./ExcelData/"+excelsheetData+".xlsx");

		XSSFSheet sheet = myBook.getSheetAt(0);
		int rowCount=sheet.getLastRowNum();
		System.out.println("rowCount="+ rowCount);
		int colCount= sheet.getRow(0).getLastCellNum();
		
		System.out.println("colCount="+ colCount);
		String[][] data =new String[rowCount][colCount];
		
		for(int i=1;i<=rowCount;i++) {
			XSSFRow row=sheet.getRow(i);
			for(int j=0;j<colCount;j++) {
				XSSFCell cell = row.getCell(j);
				String CellValue = cell.getStringCellValue();
				data[i-1][j]=CellValue;
				System.out.println(CellValue);


			}
		}
		return data;
	}



}
