package Utils;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentReportInTestNG {
	int i=1;
	public static ExtentHtmlReporter html;
	public static ExtentReports extent;
	public ExtentTest test;
	public String testCaseName,testDesc,author,category,excelsheetData;

	@BeforeSuite

	public void startResult() throws IOException {
		
		html= new ExtentHtmlReporter("./Reports/Result"+ i +".html");
		html.setAppendExisting(true);
		extent = new ExtentReports();
		extent.attachReporter(html);
		++i;
		System.out.println(i);

	}

	@BeforeMethod
	public void startTestCase() {
		test = extent.createTest(testCaseName, testDesc);
		test.assignAuthor(author);
		test.assignCategory(category);
		

	}


	public void reportSteps(String status,String desc) {
		if(status.equalsIgnoreCase("pass")) {
			test.pass(desc);
		}
		else if(status.equalsIgnoreCase("fail")){
			test.fail(desc);
		}
	}
	@AfterSuite

	public void stopResults() {
		extent.flush();
	}

}


