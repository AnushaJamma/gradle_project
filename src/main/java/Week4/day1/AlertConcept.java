package Week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AlertConcept {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text()='Try it']").click();

		driver.switchTo().alert().sendKeys("Anusha Jamma");
		driver.switchTo().alert().accept();
		WebElement text = driver.findElementByXPath("//p[@id='demo']");
		if(text.getText().contains("Anusha")) {
			System.out.println(text);
		}

	}

}
