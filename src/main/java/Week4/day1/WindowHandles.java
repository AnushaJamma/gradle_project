package Week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandles {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();

		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.manage().window().maximize();
		driver.findElementByLinkText("CONTACT US").click();
		
		Set<String> windowHandles= driver.getWindowHandles();
		List<String> eachwindow= new ArrayList<>();
		eachwindow.addAll(windowHandles);
		driver.switchTo().window(eachwindow.get(1));
		System.out.println(driver.getTitle());	
		//		driver.quit();
	}

}
