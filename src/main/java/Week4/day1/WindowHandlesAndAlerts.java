package Week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandlesAndAlerts {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		// Create object for Chrome Driver

		ChromeDriver driver = new ChromeDriver();

		driver.get("http://leaftaps.com/opentaps");

		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByXPath("//a[text()='Leads']").click();//click on leads
		driver.findElementByXPath("//a[text()='Merge Leads']").click();//click on Merge leads
		driver.findElementByXPath("//a[@class='buttonDangerous']/../../preceding::img[2]").click();

		Set<String> windowHandlesFirstIcon= driver.getWindowHandles();
		List<String> window1= new ArrayList<>();
		window1.addAll(windowHandlesFirstIcon);
		driver.switchTo().window(window1.get(1));
		System.out.println("First Icon Title: "+ driver.getTitle());
		
		String mergeLeadId= driver.findElementByXPath("//input[@name='id']").getText();//using String value finding Merged lead
		
		driver.findElementByXPath("//input[@name='id']").sendKeys("101");
		driver.findElementByXPath("//button[text()='Find Leads']").click();//click find leads
		Thread.sleep(2000);
		driver.findElementByXPath("//a[@class='linktext'][1]").click();
		Thread.sleep(2000);

		Set<String> windowHandlesSecondIcon= driver.getWindowHandles();
		List<String> window0= new ArrayList<>();
		window0.addAll(windowHandlesSecondIcon);
		driver.switchTo().window(window0.get(0));

		driver.findElementByXPath("//input[@id='ComboBox_partyIdFrom']/following::a[2]").click();

		Set<String> windowHandles1= driver.getWindowHandles();
		List<String> eachwindow2= new ArrayList<>();
		eachwindow2.addAll(windowHandles1);
		driver.switchTo().window(eachwindow2.get(1));
		System.out.println("Second Icon Title: "+ driver.getTitle());

		Thread.sleep(2000);
		
		
		driver.findElementByXPath("//input[@name='id']").sendKeys("103");
		driver.findElementByXPath("//button[text()='Find Leads']").click();//click find leads
		Thread.sleep(2000);
		driver.findElementByXPath("//a[@class='linktext'][1]").click();
		Set<String> windowHandles2= driver.getWindowHandles();
		List<String> eachwindow3= new ArrayList<>();
		eachwindow3.addAll(windowHandles2);
		driver.switchTo().window(eachwindow3.get(0));
		System.out.println(driver.getTitle());
		driver.findElementByXPath("//a[@class='buttonDangerous']").click();

		driver.switchTo().alert().accept();	
		Thread.sleep(2000);
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		driver.findElementByXPath("//input[@name='id']").sendKeys(mergeLeadId);
		if(mergeLeadId.isEmpty()) {
			System.out.println("No Lead Found");
		}
		
		else
		{
			System.out.println("Lead Found");
		}
	}

}
