package groupsTestCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;
import wdmethods.SeMethods;

public class EditLeadsGR extends ProjectMethodsForGroups{

	@Test(groups="test.Sanity",dependsOnGroups="test.smoke")
	
	public void editLeadMethod() throws InterruptedException {

		click(locateElement("xpath", "//a[contains(text(),'CRM/SFA')]"));//click CRM link

		click(locateElement("xpath","//a[text()='Leads']"));

		click(locateElement("linkText","Find Leads"));

		WebElement eleLeadId=locateElement("name","id");
		type(eleLeadId,"102");

		click(locateElement("xpath","//button[text()='Find Leads']"));

		Thread.sleep(2000);
		
		click(locateElement("xpath","(//a[@class='linktext'])[4]"));

		click(locateElement("xpath","(//a[@class='subMenuButton'])[3]"));


		WebElement descriptionEdit=locateElement("id","updateLeadForm_description");
		descriptionEdit.clear();
		type(descriptionEdit,"Editing of Description");

		WebElement importantNoteEdit=locateElement("id","updateLeadForm_importantNote");	
		importantNoteEdit.clear();
		type(importantNoteEdit,"Editing of Important Note");

		WebElement changeCompanyName=locateElement("id","updateLeadForm_companyName");
		changeCompanyName.clear();
		type(changeCompanyName,"BNY");

		WebElement clickUpadteButton=locateElement("xpath","(//input[@class='smallSubmit'])[1]");
		click(clickUpadteButton);
		Thread.sleep(2000);
		
		click(locateElement("linkText","Find Leads"));

		WebElement companyName=locateElement("xpath","(//input[@name='companyName'])[2]");
		type(companyName,"BNY");

		Thread.sleep(2000);

		WebElement leadList=locateElement("xpath","(//a[@class='linktext'])[4]");
		click(leadList);

		WebElement XactText=locateElement("xpath","//span[text()='Company Name']");

		verifyExactText(XactText, "BNY");
	}	
	@BeforeClass(groups="config")

	public void editLeadsSetData() {

		testCaseName ="EditLead";
		testDesc ="EditLead New Lead";
		author ="Anusha";
		category ="Regression";
	}
	
	@AfterMethod(groups="config")	
	public void closeEditBrowser() {
		closeBrowser();
	}
}
