package groupsTestCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CreateLeadGR extends ProjectMethodsForGroups {

	@Test(groups="test.smoke")
	
	public void createLeadMethod() throws InterruptedException {
		
		click(locateElement("xpath", "//a[contains(text(),'CRM/SFA')]"));

		click(locateElement("xpath","//a[text()='Leads']"));

		click(locateElement("xpath","//a[contains(text(),'Create Lead')]"));

		type(locateElement("id","createLeadForm_companyName"),"BNY Mellon");

		type(locateElement("id","createLeadForm_firstName"),"AnushaVin");

		type(locateElement("id","createLeadForm_lastName"),"Grandhi");

		WebElement sourceDropDown=locateElement("id","createLeadForm_dataSourceId");
		click(locateElement("id","createLeadForm_dataSourceId"));
		//selectDropDownUsingText(sourceDropDown, "Employee");

		selectDropDownUsingIndex(sourceDropDown, 4);

		WebElement industryDropDown=locateElement("id","createLeadForm_industryEnumId");
		selectDropDownUsingIndex(industryDropDown, 5);

		WebElement calender= locateElement("id","createLeadForm_birthDate-button");
		click(calender);

		WebElement clickNextButton=locateElement("xpath","//div[@class='calendar']/table/thead/tr[@class='headrow']/td[4]");
		click(clickNextButton);

		WebElement selectDate=locateElement("xpath","//div[@class='calendar']/table/tbody/tr[@class='daysrow']/td[5]");
		click(selectDate);		
		Thread.sleep(5000);

		WebElement clickCreateLead=locateElement("name","submitButton");
		click(clickCreateLead);
		Thread.sleep(2000);

	}
	
	@BeforeClass(groups="config")

	public void createLeadSetData() {

		testCaseName ="CreateLead";
		testDesc ="Create New Lead";
		author ="Anusha";
		category ="Regression";
	}

	@AfterMethod(groups="config")
	public void closingBrowser() {
		closeBrowser();

	}


}



