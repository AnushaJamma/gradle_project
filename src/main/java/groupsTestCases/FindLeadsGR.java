package groupsTestCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;
import wdmethods.SeMethods;

public class FindLeadsGR extends ProjectMethodsForGroups {
	
	@Test(groups="config")
	public void FindLeadMethod() throws InterruptedException {
	
		click(locateElement("xpath", "//a[contains(text(),'CRM/SFA')]"));//click CRM link

		click(locateElement("xpath","//a[text()='Leads']"));

		click(locateElement("linkText","Find Leads"));

		WebElement eleLeadId=locateElement("name","id");
		type(eleLeadId,"102");

		WebElement clickFindLeadsButton=locateElement("xpath","//button[text()='Find Leads']");
		click(clickFindLeadsButton);

		Thread.sleep(2000);

		WebElement leadListLink=locateElement("xpath","(//a[@class='linktext'])[4]");
		click(leadListLink);
	}
	
	@BeforeClass(groups="config")

	public void findLeadSetData() {

		testCaseName ="FindLeads";
		testDesc ="Find Lead";
		author ="Anusha";
		category ="Regression";
	}
	
	@AfterMethod(groups="config")	
	public void closingBrowser() {
		closeBrowser();
	}
}
