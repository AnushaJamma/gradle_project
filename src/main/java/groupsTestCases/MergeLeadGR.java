package groupsTestCases;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;

public class MergeLeadGR extends ProjectMethodsForGroups {
	
	@Test(groups="test.smoke")
	public void mergeLeadMethod() throws InterruptedException {

		click(locateElement("xpath", "//a[contains(text(),'CRM/SFA')]"));//click CRM link
	
		click(locateElement("xpath","//a[text()='Leads']"));

		click(locateElement("xpath","//a[text()='Merge Leads']"));//click Merge Leads

		click(locateElement("xpath","//a[@class='buttonDangerous']/../../preceding::img[2]"));

		switchToWindow(1);

		getText(driver.findElementByXPath("//input[@name='id']"));

		//String mergeLeadId= driver.findElementByXPath("//input[@name='id']").getText();//using String value finding Merged lead

		WebElement sendValue = locateElement("xpath","//input[@name='id']");
		type(sendValue,"101");

		WebElement clickFindLeads = locateElement("xpath","//button[text()='Find Leads']");
		click(clickFindLeads);//click find leads

		Thread.sleep(2000);

		WebElement clickLinkText = locateElement("xpath","//a[@class='linktext'][1]");
		click(clickLinkText);

		Thread.sleep(3000);

		switchToWindow(0);
		
		WebElement clickFirstWindowImg2 = locateElement("xpath","//input[@id='ComboBox_partyIdFrom']/following::a[2]");
		click(clickFirstWindowImg2);
	
		switchToWindow(1);

		Thread.sleep(2000);

		WebElement sendValueForImg2= locateElement("xpath","//input[@name='id']");
		type(sendValueForImg2,"103");

		WebElement clickFindLeadsImg2 = locateElement("xpath","//button[text()='Find Leads']");
		click(clickFindLeadsImg2);

		Thread.sleep(2000);

		WebElement clickLinkImg2 = locateElement("xpath","//a[@class='linktext'][1]");
		click(clickLinkImg2);

		switchToWindow(0);

		acceptAlert();

		Thread.sleep(2000);

		WebElement FindLeadsImg2 = locateElement("xpath","//a[text()='Find Leads']");
		click(FindLeadsImg2);
	}	
	/*driver.findElementByXPath("//input[@name='id']").sendKeys(mergeLeadId);
	if(mergeLeadId.isEmpty()) {
		System.out.println("No Lead Found");
	}

	else
	{
		System.out.println("Lead Found");
	}
}*/
	@BeforeClass(groups="config")

	public void findLeadSetData() {

		testCaseName ="FindLeads";
		testDesc ="Find Lead";
		author ="Anusha";
		category ="Regression";
	}
	
	@AfterMethod(groups="config")	
	public void closingBrowser() {
		closeBrowser();
	}
	}


