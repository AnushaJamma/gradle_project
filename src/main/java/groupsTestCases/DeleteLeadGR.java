package groupsTestCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;
import wdmethods.SeMethods;

public class DeleteLeadGR extends ProjectMethodsForGroups{
	@BeforeClass(groups="config")

	public void createLeadSetData() {

		testCaseName ="DeleteLead";
		testDesc ="Delete Lead Description";
		author ="Anusha";
		category ="Regression";
	}
	
	@Test(groups="test.Sanity",dependsOnGroups="test.smoke")

	public void deleteLeadsMethod() throws InterruptedException {

		click(locateElement("xpath", "//a[contains(text(),'CRM/SFA')]"));//click CRM link

		click(locateElement("xpath","//a[text()='Leads']"));

		click(locateElement("linkText","Find Leads"));

		click(locateElement("xpath","//span[text()='Phone']"));

		WebElement phoneCode=locateElement("xpath","//input[@name='phoneAreaCode']");
		type(phoneCode,"91");

		WebElement phoneNumber=locateElement("xpath","//input[@name='phoneNumber']");
		type(phoneNumber,"5454545444");

		WebElement findLeadsClick=locateElement("xpath","//button[text()='Find Leads']");
		click(findLeadsClick);

		Thread.sleep(2000);
		WebElement captureLeadId = locateElement("xpath","(//a[@class='linktext'])[4]");

		WebElement clickLeadId=locateElement("xpath","(//a[@class='linktext'])[4]");
		click(clickLeadId);

		WebElement clickDeleteButton=locateElement("xpath","//a[@class='subMenuButtonDangerous']");
		click(clickDeleteButton);

		WebElement againfindLeads=locateElement("linkText","Find Leads");
		click(againfindLeads);

		WebElement eleLeadId=locateElement("name","id");
		type(eleLeadId,"captureLeadId");

		WebElement clickFindLeadsButton=locateElement("xpath","//button[text()='Find Leads']");
		click(clickFindLeadsButton);


	}
}