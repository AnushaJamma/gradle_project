package groupsTestCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import wdmethods.SeMethods;

public class ProjectMethodsForGroups extends SeMethods {

	@Parameters({"browser","url"})

	@BeforeMethod(groups="config")

	public void login(String browser,String url) throws InterruptedException {

		startApp(browser, url);

		WebElement eleUsername = locateElement("id", "username");
		type(eleUsername, "DemoCSR");

		WebElement elePassword = locateElement("xpath", "//input[@id='password']");
		type(elePassword,"crmsfa");

		WebElement eleLogin = locateElement("xpath", "//input[@class='decorativeSubmit']");
		click(eleLogin);


	}
}

